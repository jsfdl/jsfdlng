# JSFDL NG

A tool to download files from .sfdl's

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

- git clone https://gitlab.com/jsfdl/jsfdlng.git
- import the checked out folder into intellij

### Prerequisites

Required

- Java 11 JDK

optional

- scene builder 


### Installing

Produce a executable jar containing all that is required to start
```
./gradlew shadowJar
```


## License

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

