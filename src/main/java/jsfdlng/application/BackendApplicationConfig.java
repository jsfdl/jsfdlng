package jsfdlng.application;

import jsfdlng.converter.*;
import jsfdlng.dto.SFDLBean;
import jsfdlng.dto.XmlConnectionInfo;
import jsfdlng.dto.XmlPackages;
import jsfdlng.ftp.FTPManager;
import jsfdlng.ftp.MyFTPFactory;
import jsfdlng.model.*;
import jsfdlng.model.Package;

import java.util.Set;

public class BackendApplicationConfig {

    private Converter<XmlConnectionInfo, ConnectionInfo> getXmlConnectionToConnectionConverter() {
        return new XmlConnectionToConnectionConverter();
    }

    public Converter<SFDLBean, SFDLBo> getSfdlBeanSFDLBoConverter() {
        return new SfdlToSfdlBOConverter(getXmlConnectionToConnectionConverter(), getXmlPackagesToPackagesConverter());
    }

    private Converter<XmlPackages, Packages> getXmlPackagesToPackagesConverter() {
        return new XmlPackagesToPackagesConverter();
    }

    public Converter<SFDLBo, SFDLBoView> getSfdlBoSFDLBoViewConverter(int sfdlFileHash) {
        return new SFDLBoToSFDLBOViewConverter(sfdlFileHash);
    }

    public FTPManager getFtpManager() {
        return new FTPManager(getMyFtpFactory());
    }

    private MyFTPFactory getMyFtpFactory() {
        return new MyFTPFactory();
    }
}
