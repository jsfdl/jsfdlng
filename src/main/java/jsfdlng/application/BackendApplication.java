package jsfdlng.application;

import jsfdlng.ftp.FTPManager;
import jsfdlng.model.SFDLBo;
import jsfdlng.model.SFDLBoView;
import jsfdlng.model.SFDLFileReader;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BackendApplication {

    private final BackendApplicationConfig config = new BackendApplicationConfig();
    private final SFDLFileReader sfdlFileReader = new SFDLFileReader(config.getSfdlBeanSFDLBoConverter());
    private final Map<Integer, SFDLBo> registeredBOs = new HashMap<>();
    private final FTPManager ftpManager = config.getFtpManager();

    public SFDLBoView registerBo(File sfdlfile) {
        final SFDLBo sfdlBo = sfdlFileReader.convertToBo(sfdlfile);
        registeredBOs.put(sfdlfile.hashCode(), sfdlBo);
        return config.getSfdlBoSFDLBoViewConverter(sfdlfile.hashCode()).convert(sfdlBo);
    }

    public void decryptSfdl(File file, String password) {
        final SFDLBo sfdlBo = registeredBOs.get(file.hashCode());
        sfdlBo.decrypt(password);
    }

    public SFDLBoView getViewForFile(File file) {
        final SFDLBo sfdlBo = registeredBOs.get(file.hashCode());
        return config.getSfdlBoSFDLBoViewConverter(file.hashCode()).convert(sfdlBo);
    }

    public void download(SFDLBoView selectedItem) throws IOException {
        download(selectedItem.getSfdlFileHash());
    }

    private void download(int sfdlFileHash) throws IOException {
        ftpManager.download(registeredBOs.get(sfdlFileHash));
    }
}
