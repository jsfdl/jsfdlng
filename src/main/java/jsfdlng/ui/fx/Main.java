package jsfdlng.ui.fx;

public class Main {

    public static void main(String[] args) {
        // workaround for shadowjar and javafx,
        // see https://stackoverflow.com/questions/52569724/javafx-11-create-a-jar-file-with-gradle/52571719#52571719
        FxApplication.main(args);
    }

}