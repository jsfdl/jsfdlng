package jsfdlng.ui.fx.controller;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import jsfdlng.application.BackendApplication;
import jsfdlng.model.SFDLBoView;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainAppController implements Initializable {

    private final BackendApplication backendApplication = new BackendApplication();
    private final TreeItem<SFDLBoView> rootNode = new TreeItem<>(new SFDLBoView(0));

    @FXML
    private TreeView<SFDLBoView> treeView;

    @Override
    public void initialize(final URL url, final ResourceBundle rb) {
        rootNode.setExpanded(true);
        treeView.setShowRoot(false);
    }

    @FXML
    private void handleOpenSfdl(final ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open SFDL File");
        final File sfdlFile = fileChooser.showOpenDialog(null);
        if (sfdlFile == null) {
            return;
        }

        final SFDLBoView boView = backendApplication.registerBo(sfdlFile);
        if (boView.isEncrypted()) {
            final TextInputDialog textInputDialog = new TextInputDialog();
            textInputDialog.setTitle("Password required.");
            textInputDialog.setHeaderText("I need a password to decrypt the sfdl.");
            final Optional<String> password = textInputDialog.showAndWait();
            password.ifPresent(s -> {
                        backendApplication.decryptSfdl(sfdlFile, s);
                        final SFDLBoView decryptedBoView = backendApplication.getViewForFile(sfdlFile);
                rootNode.getChildren().add(new TreeItem<>(decryptedBoView));
                        treeView.setRoot(rootNode);
                    }
            );

        } else {
            rootNode.getChildren().add(new TreeItem<>(boView));
            treeView.setRoot(rootNode);
        }
    }


    public void downloadSfdl(ActionEvent actionEvent) {
        Platform.runLater(() -> {
            final MultipleSelectionModel<TreeItem<SFDLBoView>> selectionModel = treeView.getSelectionModel();
            final ObservableList<TreeItem<SFDLBoView>> selectedItems = selectionModel.getSelectedItems();
            for (TreeItem<SFDLBoView> t : selectedItems) {
                try {
                    backendApplication.download(t.getValue());
                } catch (IOException e) {
                    final Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error in downloading...");
                    alert.setContentText(e.getLocalizedMessage());
                    alert.showAndWait();
                }
            }
        });
    }
}