package jsfdlng.ui.fx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

public class FxApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        try {
            final ClassLoader classLoader = getClass().getClassLoader();
            final URL resource = classLoader.getResource("main.fxml");
            final Parent root = FXMLLoader.load(Objects.requireNonNull(resource));
            stage.setScene(new Scene(root));
            stage.show();
        } catch (Exception e) {
            throw e;
        }
    }

}
