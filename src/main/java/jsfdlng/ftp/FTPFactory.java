package jsfdlng.ftp;


public interface FTPFactory {

    MyFTPClient createFTPClient();

}
