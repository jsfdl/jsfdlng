package jsfdlng.ftp;

public class DirectoryNotAvailableOnFtp extends Exception {
    public DirectoryNotAvailableOnFtp(String directory) {
        super("Unable to to change to directory on ftp server: " + directory);
    }
}
