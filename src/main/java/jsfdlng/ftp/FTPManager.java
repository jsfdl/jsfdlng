package jsfdlng.ftp;

import jsfdlng.model.ConnectionInfo;
import jsfdlng.model.SFDLBo;
import jsfdlng.shared.exceptions.LoginException;

import java.io.IOException;

public class FTPManager {

    private  final MyFTPFactory myFTPFactory;

    public FTPManager(MyFTPFactory myFTPFactory) {
        this.myFTPFactory = myFTPFactory;
    }

    public void download(SFDLBo sfdlBo) throws IOException {
        final MyFTPClient ftpClient = myFTPFactory.createFTPClient();
        final ConnectionInfo connectionInfo = sfdlBo.getConnectionInfo();

        final String username;
        final String password;

        if(connectionInfo.getUsername().isEmpty()) {
            username = "anonymous";
            password = "blah@blah.com";
        } else {
            username = connectionInfo.getUsername();
            password = connectionInfo.getPassword();
        }

        ftpClient.connect(connectionInfo.getHost(), connectionInfo.getPort());
        boolean login = ftpClient.login(username, password);
        if(!login) {
            throw new LoginException();
        }
        sfdlBo.getPackages().download(ftpClient);
        ftpClient.quit();
    }
}
