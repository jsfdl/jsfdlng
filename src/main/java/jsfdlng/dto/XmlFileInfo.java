package jsfdlng.dto;

import javax.xml.bind.annotation.XmlElement;

public class XmlFileInfo {

    @XmlElement(name = "FileName")
    private String fileName;

    @XmlElement(name = "DirectoryRoot")
    private String directoryRoot;

    @XmlElement(name = "DirectoryPath")
    private String directoryPath;

    @XmlElement(name = "FileFullPath")
    private String fileFullPath;

    @XmlElement(name = "FileSize")
    private long fileSize;

    @XmlElement(name = "FileHashType")
    private String fileHashType;

    @XmlElement(name = "FileHash")
    private String fileHash;

    @XmlElement(name = "PackageName")
    private String packageName;

    public String getFileName() {
        return fileName;
    }

    public String getDirectoryRoot() {
        return directoryRoot;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public String getFileFullPath() {
        return fileFullPath;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getFileHashType() {
        return fileHashType;
    }

    public String getFileHash() {
        return fileHash;
    }

    public String getPackageName() {
        return packageName;
    }

}
