package jsfdlng.dto;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class XmlPackages {

    @XmlElement(name = "SFDLPackage")
    private List<XmlSFDLPackage> xmlSfdlPackages;

    public List<XmlSFDLPackage> getPackages() {
        return xmlSfdlPackages;
    }
}
