package jsfdlng.dto;


import javax.xml.bind.annotation.XmlElement;

public class BulkFolder {

    @XmlElement(name = "BulkFolderPath")
    private String bulkFolderPath;

    @XmlElement(name = "PackageName")
    private String packageName;

    public String getBulkFolderPath() {
        return bulkFolderPath;
    }

    public String getPackageName() {
        return packageName;
    }
}
