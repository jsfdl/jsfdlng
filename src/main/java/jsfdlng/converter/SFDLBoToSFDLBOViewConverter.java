package jsfdlng.converter;

import jsfdlng.model.SFDLBo;
import jsfdlng.model.SFDLBoView;

public class SFDLBoToSFDLBOViewConverter implements Converter<SFDLBo, SFDLBoView> {

    private final int sfdlFileHash;

    public SFDLBoToSFDLBOViewConverter(int sfdlFileHash) {
        this.sfdlFileHash = sfdlFileHash;
    }

    @Override
    public SFDLBoView convert(SFDLBo sfdlBo) {
        final SFDLBoView sfdlBoView = new SFDLBoView(sfdlFileHash);
        sfdlBoView.setCaption(sfdlBo.getDescription());
        sfdlBoView.setEncrypted(sfdlBo.isEncrypted());
        return sfdlBoView;
    }
}
