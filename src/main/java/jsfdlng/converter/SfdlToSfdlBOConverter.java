package jsfdlng.converter;

import jsfdlng.dto.SFDLBean;
import jsfdlng.dto.XmlConnectionInfo;
import jsfdlng.dto.XmlPackages;
import jsfdlng.model.ConnectionInfo;
import jsfdlng.model.Package;
import jsfdlng.model.Packages;
import jsfdlng.model.SFDLBo;

import java.util.Set;

public class SfdlToSfdlBOConverter implements Converter<SFDLBean, SFDLBo> {

    private final Converter<XmlConnectionInfo, ConnectionInfo> xmlConnectionToConnectionConverter;
    private final Converter<XmlPackages, Packages> xmlPackagesToPackagesConverter;

    public SfdlToSfdlBOConverter(Converter<XmlConnectionInfo, ConnectionInfo> xmlConnectionToConnectionConverter, Converter<XmlPackages, Packages> xmlPackagesToPackagesConverter) {
        this.xmlConnectionToConnectionConverter = xmlConnectionToConnectionConverter;
        this.xmlPackagesToPackagesConverter = xmlPackagesToPackagesConverter;
    }

    @Override
    public SFDLBo convert(SFDLBean sfdlBean) {
        final SFDLBo sfdlBo = new SFDLBo(sfdlBean.getDescription(), sfdlBean.getMaxDownloadThreads(),
                sfdlBean.getSfdlFileVersion(), sfdlBean.getUploader(), sfdlBean.isEncrypted(),
                xmlConnectionToConnectionConverter.convert(sfdlBean.getXmlConnectionInfo()),
                xmlPackagesToPackagesConverter.convert(sfdlBean.getXmlPackages()));
        return sfdlBo;
    }
}
