package jsfdlng.converter;

import jsfdlng.dto.XmlConnectionInfo;
import jsfdlng.model.ConnectionInfo;

public class XmlConnectionToConnectionConverter implements Converter<XmlConnectionInfo, ConnectionInfo> {
    @Override
    public ConnectionInfo convert(XmlConnectionInfo xmlConnectionInfo) {
        return new ConnectionInfo(xmlConnectionInfo.getHost(), xmlConnectionInfo.getPort(),
                xmlConnectionInfo.getUsername(), xmlConnectionInfo.getPassword());
    }
}
