package jsfdlng.converter;

public interface Converter<T, T1> {

    T1 convert(T t);

}
