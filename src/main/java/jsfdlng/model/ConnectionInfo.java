package jsfdlng.model;

import java.util.Objects;

public class ConnectionInfo {

    private final int port;
    private String host;
    private String username;
    private String password;

    public ConnectionInfo(String host, int port, String username, String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConnectionInfo that = (ConnectionInfo) o;
        return port == that.port &&
                host.equals(that.host) &&
                username.equals(that.username) &&
                password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(port, host, username, password);
    }

    void decrypt(String password) {
        this.host = SFDLCryptor.decrypt(password, this.host);
        this.username = SFDLCryptor.decrypt(password, this.username);
        this.password = SFDLCryptor.decrypt(password, this.password);
    }

    public int getPort() {
        return port;
    }

    public String getHost() {
        return host;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
