package jsfdlng.model;

import jsfdlng.ftp.MyFTPClient;

public interface Package {
    void decrypt(String password);
    void download(MyFTPClient ftpClient);
}
