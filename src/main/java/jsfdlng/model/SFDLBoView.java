package jsfdlng.model;

public class SFDLBoView {

    private String caption;
    private static int OBJECT_COUNTER = 0;
    private final int id;
    private final int sfdlFileHash;
    private boolean isEncrypted;

    public SFDLBoView(int sfdlFileHash) {
        this.sfdlFileHash = sfdlFileHash;
        this.id = OBJECT_COUNTER++;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String blah) {
        this.caption = blah;
    }

    public void setEncrypted(boolean encrypted) {
        isEncrypted = encrypted;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " - " + caption;
    }

    public int getSfdlFileHash() {
        return sfdlFileHash;
    }
}
