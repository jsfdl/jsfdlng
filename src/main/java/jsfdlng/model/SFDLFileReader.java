package jsfdlng.model;

import jsfdlng.converter.Converter;
import jsfdlng.dto.SFDLBean;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class SFDLFileReader {

    private final Converter<SFDLBean, SFDLBo> converter;

    public SFDLFileReader(Converter<SFDLBean, SFDLBo> converter) {
        this.converter = converter;
    }

    public SFDLBo convertToBo(File file) {
        try {

            final JAXBContext jaxbContext = JAXBContext.newInstance(SFDLBean.class);
            final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            final SFDLBean sfdlBean = (SFDLBean) jaxbUnmarshaller.unmarshal(file);
            return converter.convert(sfdlBean);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }
}
