package jsfdlng.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SFDLBo {

    private String description;
    private int maxDownloadThreads;
    private Integer sfdlFileVersion;
    private String uploader;
    private boolean isEncrypted;
    private ConnectionInfo connectionInfo;
    private Packages packages;

    public Packages getPackages() {
        return packages;
    }

    public SFDLBo(String description, int maxDownloadThreads, Integer sfdlFileVersion, String uploader, boolean isEncrypted, ConnectionInfo connectionInfo, Packages packages) {
        this.description = description;
        this.maxDownloadThreads = maxDownloadThreads;
        this.sfdlFileVersion = sfdlFileVersion;
        this.uploader = uploader;
        this.isEncrypted = isEncrypted;
        this.connectionInfo = connectionInfo;
        this.packages = packages;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public void setEncrypted(boolean encrypted) {
        this.isEncrypted = encrypted;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxDownloadThreads() {
        return maxDownloadThreads;
    }

    public void setMaxDownloadThreads(int maxDownloadThreads) {
        this.maxDownloadThreads = maxDownloadThreads;
    }

    public Integer getSfdlFileVersion() {
        return sfdlFileVersion;
    }

    public void setSfdlFileVersion(Integer sfdlFileVersion) {
        this.sfdlFileVersion = sfdlFileVersion;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public ConnectionInfo getConnectionInfo() {
        return connectionInfo;
    }

    public void setConnectionInfo(ConnectionInfo connectionInfo) {
        this.connectionInfo = connectionInfo;
    }

    public void decrypt(String password) {
        if (!isEncrypted) {
            throw new RuntimeException("Not encrypted!");
        }

        this.description = SFDLCryptor.decrypt(password, this.description);
        this.uploader = SFDLCryptor.decrypt(password, this.uploader);

        connectionInfo.decrypt(password);
        packages.decrypt(password);
        isEncrypted = false;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SFDLBo sfdlBo = (SFDLBo) o;
        return maxDownloadThreads == sfdlBo.maxDownloadThreads &&
                isEncrypted == sfdlBo.isEncrypted &&
                description.equals(sfdlBo.description) &&
                sfdlFileVersion.equals(sfdlBo.sfdlFileVersion) &&
                uploader.equals(sfdlBo.uploader) &&
                connectionInfo.equals(sfdlBo.connectionInfo) &&
                packages.equals(sfdlBo.packages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, maxDownloadThreads, sfdlFileVersion, uploader, isEncrypted, connectionInfo, packages);
    }
}
