package jsfdlng.model;

import jsfdlng.ftp.MyFTPClient;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Packages {

    private final Set<Package> packages;

    public Packages(Set<Package> packages) {
        this.packages = packages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Packages packages1 = (Packages) o;
        return Objects.equals(packages, packages1.packages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packages);
    }

    public void decrypt(String password) {
        packages.forEach(p -> p.decrypt(password));
    }

    public void download(MyFTPClient ftpClient) {
        for(Package pack : packages) {
            pack.download(ftpClient);
        }
    }
}
