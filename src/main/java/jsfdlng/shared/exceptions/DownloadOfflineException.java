/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfdlng.shared.exceptions;

/**
 * @author oli
 */
public class DownloadOfflineException extends RuntimeException {

    public DownloadOfflineException(String toString) {
        super(toString);
    }

}
