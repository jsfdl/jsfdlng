module jsfdlNg {
    requires java.xml.bind;
    requires com.sun.xml.bind;
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires commons.net;
    requires org.apache.logging.log4j;

    opens jsfdlng.ui.fx to javafx.graphics;
    opens jsfdlng.ui.fx.controller to javafx.fxml;
    opens jsfdlng.dto to java.xml.bind;
}