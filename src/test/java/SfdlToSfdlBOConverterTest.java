import jsfdlng.converter.Converter;
import jsfdlng.converter.SfdlToSfdlBOConverter;
import jsfdlng.converter.XmlConnectionToConnectionConverter;
import jsfdlng.converter.XmlPackagesToPackagesConverter;
import jsfdlng.dto.SFDLBean;
import jsfdlng.dto.XmlConnectionInfo;
import jsfdlng.dto.XmlPackages;
import jsfdlng.model.ConnectionInfo;
import jsfdlng.model.Package;
import jsfdlng.model.Packages;
import jsfdlng.model.SFDLBo;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;

public class SfdlToSfdlBOConverterTest {

    @Test
    void can_convert_sfdl_to_bo() {
        //given
        final SFDLBean sfdlBean = mock(SFDLBean.class);
        final Converter<XmlConnectionInfo, ConnectionInfo> xmlConnectionInfoConnectionInfoConverter = mock(XmlConnectionToConnectionConverter.class);
        final Converter<XmlPackages, Packages> xmlPackagesToPackagesConverter = mock(XmlPackagesToPackagesConverter.class);
        final SfdlToSfdlBOConverter sut = new SfdlToSfdlBOConverter(xmlConnectionInfoConnectionInfoConverter, xmlPackagesToPackagesConverter);

        //when
        final SFDLBo result = sut.convert(sfdlBean);

        //then
        assertThat(result).isNotNull();
    }
}