import jsfdlng.model.SFDLBo;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

public class DownloaderTest {

    @Test
    void can_download_sfdlbo() {

        //given
        final SFDLBo sfdlBo = mock(SFDLBo.class);

        //when
        downloadSfdl(sfdlBo);

        //then
    }

    private void downloadSfdl(SFDLBo sfdlBo) {

        if(sfdlBo.isEncrypted()) {
            throw new RuntimeException("Must be decrypted first!");
        }



    }
}
