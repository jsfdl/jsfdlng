import jsfdlng.converter.SfdlToSfdlBOConverter;
import jsfdlng.dto.SFDLBean;
import jsfdlng.model.*;
import jsfdlng.model.Package;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

class SFDLFileReaderTest {

    @Test
    void can_parse_sfdl_file() throws URISyntaxException {

        //Given
        final File file = new File(getClass().getResource("test1.sfdl").toURI());
        final SfdlToSfdlBOConverter converter = mock(SfdlToSfdlBOConverter.class);
        final Packages packages = mock(Packages.class);
        final ConnectionInfo connectionInfo = mock(ConnectionInfo.class);
        when(converter.convert(any())).thenReturn(new SFDLBo("", 1, 1, "", false, connectionInfo, packages));
        final SFDLFileReader sut = new SFDLFileReader(converter);

        //when
        final SFDLBo sfdlBo = sut.convertToBo(file);

        //then
        verify(converter, times(1)).convert(isA(SFDLBean.class));
        assertThat(sfdlBo).isNotNull();
    }
}